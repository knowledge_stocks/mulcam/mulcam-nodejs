const http = require('http');

const server = http.createServer((req, resp) => {
  resp.writeHead(200, { 'Content-Type': 'text/html; charset=UTF-8' });
  switch (req.url) {
    case '/': {
      resp.write('여기는 루트입니다.');
      break;
    }
    case '/login': {
      resp.write('여기는 로그인입니다.');
      break;
    }
    case '/member': {
      resp.write('여기는 회원정보입니다.');
      break;
    }
    default: {
      resp.writeHead(404, { 'Content-Type': 'text/html; charset=UTF-8' });
      resp.write('잘못된 경로입니다.');
      break;
    }
  }
  resp.end();
});

// 클라이언트 요청을 기다림
server.listen(8080, '0.0.0.0', function () {
  console.log('8080번 포트로 리스닝 시작');
});
