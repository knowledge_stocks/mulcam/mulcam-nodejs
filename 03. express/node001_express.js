const express = require('express');
const { appendFile } = require('fs');

const server = express();

server.get('/', (req, res) => {
  res.send(`
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <p>여기는 루트입니다.</p>
  </body>
</html>`);
});

server.get('/login', (req, res) => {
  res.send(`
<!DOCTYPE html>
<html lang="ko">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <p><input type="text" placeholder="아이디" /></p>
    <p><input type="text" placeholder="비밀번호" /></p>
    <p><input type="submit" value="확인" /></p>
  </body>
</html>
`);
});

server.get('/member', (req, res) => {
  res.send('여기는 회원정보입니다.');
});

server.listen(8080, '0.0.0.0');
