const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const path = require('path');

const indexRouter = require('./routers/index');
const memberRouter = require('./routers/member');

const app = express();

// static 경로 잡아주기
app.use(express.static(path.join(__dirname, 'static')));

// view 엔진과 파일 경로 지정
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views/ejs'));

// express-ejs-layouts 설정과 해당 파일이 views의 어디에 있는지 경로 지정
app.use(expressLayouts);
app.set('layout', 'common/layout');

// post의 데이터를 파싱해서 받아오기 위한 작업이라고 한다.
app.use(express.json());
app.use(express.urlencoded());

// router 설정
app.use('/', indexRouter);
app.use('/member', memberRouter);

// 서버 시작
app.listen(8080, '0.0.0.0', () => {
  console.log('서버 시작');
});
