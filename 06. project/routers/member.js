const { Router } = require('express');

const dbPool = require('../db/pool');

const router = Router();

router.get('/listMembers', async (req, res) => {
  const [rows, fields] = await dbPool.query('SELECT * FROM t_member');
  res.render('member/listMembers.ejs', { name: '관리자', memberList: rows });
});

router.get('/memberForm', (req, res) => {
  res.render('member/memberForm.ejs', { name: '관리자' });
});

router.post('/addMember', async (req, res) => {
  let body = req.body;

  // await dbPool.execute(
  //   'INSERT INTO t_member t_member(id,pwd,name,email) VALUES(?,?,?,?)',
  //   [body.id, body.pwd, body.name, body.email]
  // );

  // 위처럼 하는 대신 간단하게 아래처럼 처리할 수 있다.
  // 근데 이건 위험한게 가입 시간과 같은 데이터도 body에 입력된 값으로 들어가기 때문에 사용하면 안 될 것 같다.
  // await dbPool.execute('INSERT INTO t_member SET ?', body);

  // 요렇게 하는게 안전할 것 같다.
  // 근데 이상하게 execute로 하면 안되고 query로 해야 된다.
  // execute는 prepared statement라고 한다.
  let data = { id: body.id, pwd: body.pwd, name: body.name, email: body.email };
  await dbPool.query('INSERT INTO t_member SET ?', data);

  res.redirect('/member/listMembers');
});

router.get('/removeMember', async (req, res) => {
  let id = req.query.id;
  await dbPool.execute('DELETE FROM t_member WHERE id=?', [id]);

  res.redirect('/member/listMembers');
});

router.get('/modMemberForm/:id', async (req, res) => {
  let id = req.params.id;

  const [rows] = await dbPool.query('SELECT * FROM t_member WHERE id=?', [id]);

  res.render('member/modMember.ejs', { name: '관리자', member: rows[0] });
});

router.post('/modMember', async (req, res) => {
  let body = req.body;

  await dbPool.execute(
    'UPDATE t_member SET pwd=:pwd, name=:name, email=:email WHERE id=:id',
    body
  );

  res.redirect('/member/listMembers');
});

module.exports = router;
