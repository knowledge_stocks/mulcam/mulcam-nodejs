const { Router } = require('express');

const router = Router();

router.get('/', (req, res) => {
  // render 함수로 view와 model을 설정해줌
  res.render('home.ejs', { name: '관리자' });
});

module.exports = router;
