/* console.dir(object): 자바 스크립트 객체의 속성을 출력
console.time(id): 실행시간 측정을 측정하기 위한 시작시간을 기록
console.ltimeEnd(id): 실행시간을 측정하기 위한 끝 시간을 기록 */

const obj = {
  outside: {
    inside: {
      key: "value",
    },
  },
};
console.dir(obj);
// 실행결과: { outside: { inside: { key: 'value' } } }
console.dir(obj, { colors: false });
// 실행결과에서 색상이 사라짐
console.dir(obj, { depth: 1 });
// 실행결과: { outside: { inside: [Object] } }

console.time("id");
for (let i = 0; i < 100000; i++) {
  continue;
}
console.timeEnd("id");
// 실행결과: id: 26.083ms
