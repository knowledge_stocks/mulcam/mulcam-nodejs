function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

(async function () {
  const promise1 = Promise.resolve('성공1');
  const promise2 = Promise.resolve('성공2');
  const promise3 = Promise.resolve('성공3');
  const promise4 = Promise.resolve('성공4');
  const promise5 = Promise.reject('실패5');

  console.log('all: 하나라도 실패하면 전체 실패로 봄');
  Promise.all([promise1, promise2, promise3, promise4, promise5])
    .then((msgs) => {
      console.log(msgs);
    })
    .catch((errors) => {
      console.log(errors);
    });

  await sleep(100);

  console.log();
  console.log(
    'allSettled: 성공이건 실패건 다 수행하고 then으로 status와 함께 결과를 반환함'
  );
  Promise.allSettled([promise1, promise2, promise3, promise4, promise5]).then(
    (msgs) => {
      console.log(msgs);
    }
  );

  await sleep(100);

  console.log();
  console.log('race: 성공이건 실패건 그냥 먼저 수행된게 출력됨');
  Promise.race([promise1, promise2, promise3, promise4, promise5])
    .then((msgs) => {
      console.log(msgs);
    })
    .catch((errors) => {
      console.log(errors);
    });
})();
