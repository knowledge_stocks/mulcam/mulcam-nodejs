'use strict';

const express = require('express');

const server = express();

// 뷰 위치 설정
server.set('views', './views');
// 뷰 엔진 설정
server.set('view engine', 'ejs');

// 라우터 위치 지정
const router = require('./routes/router');

server.use('/', router);

server.listen(8080, '0.0.0.0', () => {
  console.log('서버 시작');
});
