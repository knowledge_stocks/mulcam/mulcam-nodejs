/* process: 프로세스 실행에 대한 정보를 다루는 객체 */

// 실행환경 정보들이 좌르륵 출력된다.
console.log(process);

// 실행할 때 전달한 인자값들
console.log(process.argv);

// 환경변수
console.log(process.env);

// 프로세스 강제 종료
process.exit();
