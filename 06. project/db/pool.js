const mysql = require('mysql2');

const pool = mysql.createPool({
  host: '192.168.56.201',
  port: '13306',
  database: 'mydb',
  user: 'root',
  password: 'asdf1234',
  debug: false,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
  namedPlaceholders: true,
});

const promisePool = pool.promise();

module.exports = promisePool;
