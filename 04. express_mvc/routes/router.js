'use strict';

const { Router } = require('express');

const router = Router();

router.get('/', (req, res) => {
  // 뷰 엔진을 통해 문서를 렌더링한다.
  res.render('index');
});

router.get('/login', (req, res) => {
  res.render('login');
});

router.get('/member', (req, res) => {
  res.send('여기는 회원정보입니다.');
});

module.exports = router;
